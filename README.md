Thank you scrooloose. But I'll do it without the horse :).

Installation
============

Clone the repo:
`git clone https://github.com/dajoen/vimfiles.git ~/.vim`

Grab the plugin submodules:
`cd ~/.vim && git submodule init && git submodule update`


Make sure vim finds the vimrc file by either symlinking it:
`ln -s ~/.vim/vimrc ~/.vimrc`

or by sourcing it from  your own ~/.vimrc:
`source ~/.vim/vimrc`

Cygwin
======

When you're on cygwin I would advise to install ruby as well via RVM.

Puppet
======

The puppet plugin works on cygwin as well. Install the puppet and puppet-lint gems as well to enable syntax checking of puppet code.

Install rvm:
`curl -sSL https://get.rvm.io | bash -s stable --ruby`

Install puppet:
`gem install puppet`

Install puppet lint:
`gem install puppet-lint`
